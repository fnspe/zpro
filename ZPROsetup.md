# ZPRO setup  
Shrnutí nastavení softwarů z první hodiny ZPRO.

## Slack
[Slack](https://slack.com/intl/en-cz/) je komunikační platforma pro týmy. Její obdobou jsou např. 
[Microsoft Teams](https://products.office.com/en-US/microsoft-teams/group-chat-software). My však budeme používat 
 [desktop](https://slack.com/intl/en-cz/lp/three?cvosrc=ppc.bing.d_ppc_bing_eastern-europe_en_brand-hv&cvo_creative=%7Bcreative%7D&utm_medium=ppc&utm_source=bing&utm_campaign=d_ppc_bing_eastern-europe_en_brand-hv&utm_term=slack&cvosrc=ppc.bing.slack&cvo_campaign=&cvo_crid=%7Bcreative%7D&Matchtype=e&utm_source=bing&utm_medium=ppc&c3api=5523,%7Bcreative%7D,slack&msclkid=6d828a84f5d61c0ef4f2d70e545eddc2&gclid=CMaQgbzJ6uQCFZNnGwod8iwN9A&gclsrc=ds) 
 verzi Slacku a podle osobní potřeby pak i [Android](https://slack.com/intl/en-cz/downloads/android) či [iOS](https://slack.com/intl/en-cz/downloads/ios) verzi. 
 
 Prvním krokem je tedy vytvoření účtu. V mailu, kterým jste se zapsali na prvním cvičení, naleznete odkaz na přihlášení.
 Založte si svůj účet a přihlaste se do workspace ZPRO. Je doporučeno stáhnout si i mobilní aplikaci a používat Slack i
 pro komunikaci mimo ZPRO. To je však plně na individuálním zvážení - není to nutné!
 
 Ve workspace ZPRO máte zřízen přístup k channel `#general` kde naleznete i spolužáky z některých jiných skupin 
 (`ZS 6`, `JI B`, `JCH 1`, `JCH 2` a `JI C`). Dále máte přístup do channelu svého kroužku a samozřejmě můžete komunikovat mezi sebou navzájem.
 
 Ke komunikaci tak budeme využívat výhradně Slack, vytvoříme si tak svůj privátní [stackoverflow](https://stackoverflow.com). 
 O tom se vám cvičící určitě zmíní. Velice brzy tak zjistíte proč mail není ideální komunikace a to zvláště pro cvičení typu ZPRO.
 

## GitLab
Druhý obdobným krokem je vytvoření účtu na [GitLabu](https://gitlab.com/users/sign_in#register-pane). GitLab je software 
pro správu kódu a veřte, že Vám ušetří spoustu starostí a práce. Jeho funkce začneme využívat hned jak se trochu seznámíme s 
jazykem `C++`.  

## Git (Windows only)  
Uživatelé Linuxových distribucí a mac OS nemusí tento krok provádět. Potřebný software je předinstalován. 
Uživatelé Windows musí doinstalovat kompilátor `C`/`C++` a `git`. 

Instalační balík `git` pro Windows naleznete [zde](
https://github.com/git-for-windows/git/releases/download/v2.23.0.windows.1/Git-2.23.0-64-bit.exe).

Pro kompilátor `C`/`C++` je více možnosí a jsou popsané 
[zde](https://www.jetbrains.com/help/clion/quick-tutorial-on-configuring-clion-on-windows.html). 
Stačí si zvolit jeden a postupovat podle návodu. Např. Cygwin lze stáhnout [tady](https://cygwin.com/install.html)
Rozbalit a zvolit k instalaci `gcc-g++`, `make`, `cmake` a `gdb`.

## CLion
CLion je takzvané IDE (Integrated Development Environment) čili vývojové prostředí. Slouží pro zjednodušení 
vývoje kódu. Doporučujeme používat IDE CLion . Je to placený software. Studenti FJFI 
mají k dispozici plnou verzi pro studijní účely. Pro její získání je potřeba si vytvořit účet na stránkach JetBrains 
[zde](https://www.jetbrains.com/shop/eform/students). Pro vytvoření účtu se musí použít školní email FJFI. CLion 
je ke stažení [zde](https://www.jetbrains.com/clion/download). Po nainstalovaní je pro aktivaci nutno se přihlásit 
JetBrains účtem. 

### Link Toolchains (Windows only)
Uživatelé Windows musí nalinkovat nainstalovaný kompilátor. V CLion na Windowse se spouští nastavení skratkou 
`Ctrl+Alt+S`. V nastaveních je sekce Build, Execution, Deployment a po kliknutí na Toolchains a navolení příslušného 
kompilátoru by měl CLion automaticky rozpoznat nainstalovaný kompilátor.



